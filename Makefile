CC := gcc
CFLAGS := -std=c11 -Wall -Wextra -g
LDLIBS := -ldl

fakeid.so: fakeid.c
	$(CC) $(CFLAGS) -shared -fPIC $(LDLIBS) -o $@ $<

.PHONY: clean
clean:
	rm -rf fakeid.so
